//
//  WeatherForecastService.h
//  Weather
//
//  Created by Mindfire on 25/08/18.
//

#import <Foundation/Foundation.h>
#import "BaseService.h"
#import "WeatherRequest.h"

@interface WeatherForecastService : BaseService
+ (instancetype) sharedInstance;
- (void) getWeatherForecastBy:(WeatherRequest *)weather withCompletionHandler:(ResponseBlock) completionHandler;
@end
