//
//  BaseService.h
//  Weather
//
//  Created by Mindfire on 24/08/18.
//

#import <Foundation/Foundation.h>

@interface BaseService : NSObject

typedef void (^ResponseBlock)(id,NSError*);
+(NSString *)getServiceUrlFrom:(NSString *)key;

@end
