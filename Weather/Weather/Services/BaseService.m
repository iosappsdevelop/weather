//
//  BaseService.m
//  Weather
//
//  Created by Mindfire on 24/08/18.
//

#import "BaseService.h"

@implementation BaseService
static NSString * const kBaseUrl = @"base_url";
static NSString * const kEndpoints = @"Endpoints";

+ (NSString *)getServiceUrlFrom:(NSString *)key {
    //    Read data from this plist file -
    
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"WebserviceList" ofType:@"plist"];
    NSDictionary *list = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    
    NSString *baseUrlStr = [list objectForKey:kBaseUrl];
    NSDictionary *endpointsDict = [list objectForKey:kEndpoints];
    if([endpointsDict objectForKey:key]) {
        return [NSString stringWithFormat:@"%@/%@",baseUrlStr,[endpointsDict objectForKey:key]];
    }
    return nil;
}


@end
