//
//  WeatherForecastService.m
//  Weather
//
//  Created by Mindfire on 25/08/18.
//

#import "WeatherForecastService.h"
#import "WebRequestManager.h"
#import "WeatherData.h"

@implementation WeatherForecastService

+ (instancetype) sharedInstance {
    static WeatherForecastService *weatherForecastService;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        weatherForecastService = [[WeatherForecastService alloc] init];
    });
    return weatherForecastService;
}

- (void) getWeatherForecastBy:(WeatherRequest *)weather withCompletionHandler:(ResponseBlock) completionHandler {
    NSDictionary *dictionary = [weather getDictionaryFromWeather];
    [WebRequestManager getWebserviceRequestWithUrlStr:[BaseService getServiceUrlFrom:@"get_forecast"] WithParams:dictionary WithResult:^(id result, NSError *error) {
        WeatherData *weatherData = [[WeatherData alloc] initWithDictionary:result];
        completionHandler(weatherData, error);
    }];
}

@end
