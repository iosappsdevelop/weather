//
//  UtilityManager.m
//  Weather
//
//  Created by Mindfire on 25/08/18.
//

#import "UtilityManager.h"

@implementation UtilityManager
+ (NSString *) converKelvinToCelsius:(NSString *) temperatureKelvin {
    NSNumber *temperatureInCelcius = [NSNumber numberWithInteger:[temperatureKelvin integerValue]-272.15];
    return [NSString stringWithFormat:@"%@%@",[temperatureInCelcius stringValue], @"\u00B0"];
}

+ (NSString *) convertDateToDay :(NSDate *) date{
    NSCalendar * calendar = [NSCalendar currentCalendar];
    NSDateComponents * dateComponents = [calendar components:NSCalendarUnitWeekday fromDate: date];
    return calendar.shortWeekdaySymbols[dateComponents.weekday - 1];

}

+ (NSArray *) getCityArray {
    NSString *pathToCity = [[NSBundle mainBundle] pathForResource:@"city.list" ofType:@"json"];
    NSURL *localFileURL = [NSURL fileURLWithPath:pathToCity];
    NSData *cityData = [NSData dataWithContentsOfURL:localFileURL];
    
    return [NSJSONSerialization JSONObjectWithData:cityData options:NSJSONReadingMutableContainers error:nil];
}

@end
