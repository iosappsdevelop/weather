//
//  LocationUtil.h
//  Weather
//
//  Created by Mindfire on 26/08/18.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@protocol MapDelegate <NSObject>
@required
- (void) currentCity: (NSString *) city;

@end

@interface LocationUtil : NSObject <MKMapViewDelegate, CLLocationManagerDelegate>

@property (nonatomic,weak) id<MapDelegate> delegate;

@end
