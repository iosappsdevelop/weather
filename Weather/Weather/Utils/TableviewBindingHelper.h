//
//  TableviewBindingHelper.h
//  Weather
//
//  Created by Mindfire on 24/08/18.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "WeatherViewDelegate.h"
#import "WeatherListViewModel.h"

@interface TableviewBindingHelper : NSObject <UITableViewDelegate, UITableViewDataSource, WeatherViewDelegate>

@property (weak, nonatomic) id<UITableViewDelegate> delegate;

+ (instancetype)bindingHelperForTableView:(UITableView *)tableView withViewModel:(WeatherListViewModel *) viewModel
                             templateCell:(UINib *)templateCellNib;
- (void) updateWeatherResult;
@end
