//
//  LocationUtil.m
//  Weather
//
//  Created by Mindfire on 26/08/18.
//

#import "LocationUtil.h"

@implementation LocationUtil {
    CLLocationManager *_locationManager;
}

- (LocationUtil *) init {
    self = [super init];
    if(self) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        [_locationManager requestWhenInUseAuthorization];
    }
    return self;
}

- (void) locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if(status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [_locationManager startUpdatingLocation];
    }
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    [self getReverseGeocode:locations];
    [_locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [_locationManager stopUpdatingLocation];
}

/**
 To fetch placemark or address from current latitude and longitude.
 @param locations array of locations having current locaton at 0th index.
 */
- (void) getReverseGeocode: (NSArray<CLLocation *>*) locations {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    if(locations.count > 0) {
        CLLocationCoordinate2D myCoOrdinate;
        
        myCoOrdinate.latitude = [locations objectAtIndex:0].coordinate.latitude;
        myCoOrdinate.longitude = [locations objectAtIndex:0].coordinate.longitude;
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:myCoOrdinate.latitude longitude:myCoOrdinate.longitude];
        [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error){
            if (error) {
                NSLog(@"failed with error: %@", error);
                return;
            }
            if(placemarks.count > 0) {
                CLPlacemark* placemark = [placemarks objectAtIndex:0];
                [self.delegate currentCity:placemark.locality];
            }
        }];
    }
}
@end
