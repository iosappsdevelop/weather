//
//  UtilityManager.h
//  Weather
//
//  Created by Mindfire on 25/08/18.
//

#import <Foundation/Foundation.h>

@interface UtilityManager : NSObject

+(NSString *) converKelvinToCelsius:(NSString *) temperatureKalvin;
+ (NSString *) convertDateToDay :(NSDate *) date;
+ (NSArray *) getCityArray;
@end
