//
//  TableviewBindingHelper.m
//  Weather
//
//  Created by Mindfire on 24/08/18.
//

#import "TableviewBindingHelper.h"

@implementation TableviewBindingHelper {
    UITableView *_tableView;
    NSArray *_data;
    WeatherListViewModel * _viewModel;
    UITableViewCell *_templateCell;
}

#pragma  mark - initialization

- (instancetype)initWithTableView:(UITableView *)tableView withViewModel:(WeatherListViewModel *) viewModel
                     templateCell:(UINib *)templateCellNib {
    
    if (self = [super init]) {
        _tableView = tableView;
        _data = [NSArray array];
        _viewModel = viewModel;
        _viewModel.delegate = self;
        
        // create an instance of the template cell and register with the table view
        _templateCell = [[templateCellNib instantiateWithOwner:nil options:nil] firstObject];
        [_tableView registerNib:templateCellNib forCellReuseIdentifier:_templateCell.reuseIdentifier];
        
        // use the template cell to set the row height
        _tableView.rowHeight = _templateCell.bounds.size.height;
        
        _tableView.dataSource = self;
        _tableView.delegate = self;

    }
    return self;
}

+ (instancetype)bindingHelperForTableView:(UITableView *)tableView withViewModel:(WeatherListViewModel *) viewModel
                             templateCell:(UINib *)templateCellNib {
    return [[TableviewBindingHelper alloc] initWithTableView:tableView withViewModel:viewModel
                                                  templateCell:templateCellNib];
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id<WeatherViewDelegate> cell = [tableView dequeueReusableCellWithIdentifier:_templateCell.reuseIdentifier];
    [cell bindViewModel:_data[indexPath.row]];    
    return (UITableViewCell *)cell;
}

#pragma mark = UITableViewDelegate forwarding

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // forward the delegate method
    if ([self.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)]) {
        [self.delegate tableView:tableView didSelectRowAtIndexPath:indexPath];
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([self.delegate respondsToSelector:@selector(scrollViewDidScroll:)]) {
        [self.delegate scrollViewDidScroll:scrollView];
    }
}

- (BOOL)respondsToSelector:(SEL)aSelector {
    if ([self.delegate respondsToSelector:aSelector]) {
        return YES;
    }
    return [super respondsToSelector:aSelector];
}

- (id)forwardingTargetForSelector:(SEL)aSelector {
    if ([self.delegate respondsToSelector:aSelector]) {
        return self.delegate;
    }
    return [super forwardingTargetForSelector:aSelector];
}

#pragma mark WeatherViewDelegate
- (void) updateWeatherResult {
    self->_data = _viewModel.results;
    [self->_tableView reloadData];
}


@end
