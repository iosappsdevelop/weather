//
//  CoreDataUtil.h
//  Weather
//
//  Created by Mindfire on 25/08/18.
//

#import <Foundation/Foundation.h>

@interface CoreDataUtil : NSObject

+ (instancetype)getInstance;

+ (void)installDatabaseWithName:(NSString *)name;

- (NSArray*)fetchEntityWithName:(NSString *)name
                  withCondition:(NSPredicate *)condition
              withSortCondition:(NSArray*)sortConditions
                      withLimit:(NSInteger)limit;

- (NSArray*)fetchEntityWithName:(NSString *)name
                  withCondition:(NSPredicate *)condition
              withSortCondition:(NSArray*)sortConditions
                      withLimit:(NSInteger)limit
                  withBindClass:(NSString*)className;

- (BOOL)updateEntity:(NSString*)name
       withCondition:(NSPredicate*)condition
           withValue:(NSDictionary*)values;

- (BOOL)deleteEntity:(NSString*)name withCondition:(NSPredicate *)condition;

@end
