//
//  WeatherViewDelegate.h
//  Weather
//
//  Created by Mindfire on 24/08/18.
//

#import <Foundation/Foundation.h>

/// A protocol which is adopted by views which are backed by view models.
@protocol WeatherViewDelegate <NSObject>

@optional
/// Binds the given view model to the view
- (void)bindViewModel:(id)viewModel;
- (void)updateWeatherResult;

@end
