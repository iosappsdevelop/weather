//
//  City.h
//  Weather
//
//  Created by Mindfire on 25/08/18.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSManagedObject;

@interface City : NSObject

@property (nonatomic, strong) NSString *cityId;
@property (nonatomic, strong) NSString *cityName;
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *country;


+ (instancetype)taskWithManagedObject:(NSManagedObject*)object;
+ (instancetype)intiWithDictioinary:(NSDictionary *) dict;
- (NSDictionary*)dictionary;
@end
