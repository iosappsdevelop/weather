//
//  ForecastData.m
//  Weather
//
//  Created by Mindfire on 25/08/18.
//

#import "ForecastData.h"

@implementation ForecastData

NSString *const kTempMin = @"min";
NSString *const kTempMax = @"max";
NSString *const kDayTemp = @"day";
NSString *const kDate = @"dt";
NSString *const kForeTemp = @"temp";

- (instancetype) initWithDictionary:(NSDictionary *)data {
    self = [super init];
    if(self) {
        NSDictionary *tempDict = [data objectForKey:kForeTemp];
        self.minTemp = [tempDict objectForKey:kTempMin];
        self.maxTemp = [tempDict objectForKey:kTempMax];
        self.temp = [tempDict objectForKey:kDayTemp];
        self.date = [data objectForKey:kDate];
    }
    return self;
}

@end
