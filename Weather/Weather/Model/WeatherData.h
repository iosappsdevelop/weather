//
//  WeatherData.h
//  Weather
//
//  Created by Mindfire on 25/08/18.
//

#import <Foundation/Foundation.h>
#import "ForecastData.h"

@interface WeatherData : NSObject

@property NSString *cityName;
@property NSString *temp;
@property NSString *weatherDescription;
@property NSMutableArray<ForecastData *> *forecastData;

- (instancetype) initWithDictionary:(NSDictionary *)data;
@end
