//
//  ForecastData.h
//  Weather
//
//  Created by Mindfire on 25/08/18.
//

#import <Foundation/Foundation.h>

@interface ForecastData : NSObject
@property NSString* minTemp;
@property NSString* maxTemp;
@property NSString* temp;
@property NSString* date;

- (instancetype) initWithDictionary:(NSDictionary *)data;
@end
