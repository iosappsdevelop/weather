//
//  WeatherData.m
//  Weather
//
//  Created by Mindfire on 25/08/18.
//

#import "WeatherData.h"

@implementation WeatherData

NSString *const kList = @"list";
NSString *const kCity = @"city";
NSString *const kCityName = @"name";
NSString *const kTemp = @"temp";
NSString *const kDay = @"day";
NSString *const kWeather = @"weather";
NSString *const kDescription = @"description";

- (instancetype) initWithDictionary:(NSDictionary *)data {
    self = [super init];
    if(self) {
        self.cityName = [[data objectForKey:kCity] objectForKey:kCityName];
        NSArray *listArray = [data objectForKey:kList];
        self.temp = [[[listArray firstObject] objectForKey:kTemp]objectForKey:kDay];
        self.weatherDescription = [[[[listArray firstObject] objectForKey:kWeather] firstObject] objectForKey:kDescription];
        self.forecastData = [[NSMutableArray alloc] init];
        for(NSDictionary *dict in listArray) {
            [self.forecastData addObject:[[ForecastData alloc] initWithDictionary:dict]];
        }
    }
    return self;
}

@end
