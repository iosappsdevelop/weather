//
//  WeatherRequest.h
//  Weather
//
//  Created by Mindfire on 25/08/18.
//

#import <Foundation/Foundation.h>

@interface WeatherRequest : NSObject

@property NSString* cityId;

- (NSDictionary *) getDictionaryFromWeather;


@end
