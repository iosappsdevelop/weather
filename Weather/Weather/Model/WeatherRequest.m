//
//  WeatherRequest.m
//  Weather
//
//  Created by Mindfire on 25/08/18.
//

#import "WeatherRequest.h"
#import "Constants.h"

@implementation WeatherRequest

NSString *const kCityId = @"id";
NSString *const kAppId = @"appid";

- (NSDictionary *) getDictionaryFromWeather {
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    [dictionary setObject:self.cityId forKey:kCityId];
    [dictionary setObject:kApiKey forKey:kAppId];
    return dictionary;
}

@end
