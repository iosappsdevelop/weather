//
//  City.m
//  Weather
//
//  Created by Mindfire on 25/08/18.
//

#import "City.h"

@implementation City

NSString *const kId = @"id";
NSString *const kName = @"name";
NSString *const kCityLatitude = @"lat";
NSString *const kCityLongitude = @"lon";
NSString *const kCountry = @"country";
NSString *const kCoord = @"coord";

+ (instancetype)taskWithManagedObject:(NSManagedObject *) object{
    City *instance = [[City alloc]init];
    
    if (instance) {
        instance.cityId = [NSString stringWithFormat:@"%@",[object valueForKey:kId]];
        instance.cityName = [object valueForKey:kName];
        instance.latitude = [NSString stringWithFormat:@"%@",[[object valueForKey:kCoord] valueForKey: kCityLatitude]];
        instance.longitude = [NSString stringWithFormat:@"%@",[[object valueForKey:kCoord] valueForKey:kCityLongitude]];
        instance.country = [object valueForKey:kCountry];

    }
    
    return instance;
}

+ (instancetype)intiWithDictioinary:(NSDictionary *) dict {
    if([dict isKindOfClass:[City class]])
        return (City *)dict;
    
    City *instance = [[City alloc]init];
    
    if (instance) {
        instance.cityId = [NSString stringWithFormat:@"%@",[dict valueForKey:kId]];
        instance.cityName = [dict valueForKey:kName];
        instance.latitude = [NSString stringWithFormat:@"%@",[dict valueForKey: kCityLatitude]];
        instance.longitude = [NSString stringWithFormat:@"%@",[dict valueForKey:kCityLongitude]];
        
    }
    
    return instance;
}


- (NSDictionary *)dictionary {
    return @{
             kId : [NSString stringWithFormat:@"%@",self.cityId],
             kName : self.cityName,
             kCityLongitude : [NSString stringWithFormat:@"%@",self.latitude],
             kCityLatitude : [NSString stringWithFormat:@"%@",self.longitude]
             };
}

@end
