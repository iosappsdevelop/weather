//
//  NSDictionary+UrlEncoding.h
//  Weather
//
//  Created by Mindfire on 25/08/18.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (UrlEncoding)
-(NSString*) urlEncodedString;
@end
