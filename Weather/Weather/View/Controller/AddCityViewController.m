//
//  AddCityViewController.m
//  Weather
//
//  Created by Mindfire on 26/08/18.
//

#import "AddCityViewController.h"
#import "CityListViewModel.h"
#import "TableviewBindingHelper.h"

@interface AddCityViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (strong, nonatomic) CityListViewModel *viewModel;
@property (strong, nonatomic) TableviewBindingHelper *bindingHelper;

@end

@implementation AddCityViewController
static NSString * const kCityCell = @"CityCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.viewModel = [CityListViewModel new];
    UINib *nib = [UINib nibWithNibName:kCityCell bundle:[NSBundle mainBundle]];

    self.bindingHelper = [TableviewBindingHelper bindingHelperForTableView:self.tableView withViewModel:self.viewModel
                                                              templateCell:nib];
    self.viewModel.delegate = self;
    self.bindingHelper.delegate = self;
    self.searchBar.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.searchBar resignFirstResponder];
    [self.viewModel citySelected:indexPath.row];
}

#pragma mark - WeatherViewDelegate methods
- (void) updateWeatherResult {
    self.tableView.hidden = self.viewModel.isTableViewHidden;
    self.addButton.enabled = self.viewModel.isAddButtonEnabled;
    UIColor *color = self.viewModel.isAddButtonEnabled ? [UIColor blueColor] : [UIColor grayColor];
    [self.addButton setTitleColor:color forState:UIControlStateNormal];
    [self.searchBar setText:self.viewModel.searchString];
    [self.bindingHelper updateWeatherResult];
}

#pragma mark - UISearchBarDelegate methods
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self.viewModel setSearchText:searchText];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (IBAction)addClicked:(id)sender {
    [self.viewModel addClicked];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
