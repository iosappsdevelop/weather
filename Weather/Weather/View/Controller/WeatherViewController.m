//
//  WeatherViewController.m
//  Weather
//
//  Created by Mindfire on 23/08/18.
//

#import "WeatherViewController.h"
#import "ForecastDetailCell.h"
#import "TableviewBindingHelper.h"
#import "WeatherListViewModel.h"
#import "WeatherForecastService.h"

@interface WeatherViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) WeatherListViewModel *viewModel;
@property (strong, nonatomic) TableviewBindingHelper *bindingHelper;

@end

@implementation WeatherViewController
static NSString * const kForecastDetailCell = @"ForecastDetailCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewModel = [WeatherListViewModel new];
    UINib *nib = [UINib nibWithNibName:kForecastDetailCell bundle:[NSBundle mainBundle]];
    self.bindingHelper = [TableviewBindingHelper bindingHelperForTableView:self.tableView withViewModel:self.viewModel
                                           templateCell:nib];
    self.bindingHelper.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.viewModel getCurrentLocation];
    [self.viewModel reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
