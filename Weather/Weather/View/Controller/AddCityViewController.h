//
//  AddCityViewController.h
//  Weather
//
//  Created by Mindfire on 26/08/18.
//

#import <UIKit/UIKit.h>
#import "WeatherViewDelegate.h"

@interface AddCityViewController : UIViewController <UITableViewDelegate, UISearchBarDelegate ,WeatherViewDelegate>

@end
