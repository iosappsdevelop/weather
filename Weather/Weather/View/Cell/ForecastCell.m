//
//  ForecastCell.m
//  Weather
//
//  Created by Mindfire on 25/08/18.
//

#import "ForecastCell.h"

@interface ForecastCell()

@property (weak, nonatomic) IBOutlet UILabel *minTempLbl;
@property (weak, nonatomic) IBOutlet UILabel *maxTempLbl;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UILabel *dayLbl;

@end
@implementation ForecastCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)bindViewModel:(ForecastCellViewModel *)viewModel {
    [self.minTempLbl setText:viewModel.minTempString];
    [self.maxTempLbl setText:viewModel.maxTempString];
    [self.dateLbl setText:viewModel.dateString];
    [self.dayLbl setText:viewModel.dayString];
}

@end
