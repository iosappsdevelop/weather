//
//  ForecastCell.h
//  Weather
//
//  Created by Mindfire on 25/08/18.
//

#import <UIKit/UIKit.h>
#import "WeatherViewDelegate.h"
#import "ForecastCellViewModel.h"

@interface ForecastCell : UICollectionViewCell <WeatherViewDelegate>

@end
