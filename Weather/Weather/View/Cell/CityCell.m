//
//  CityCell.m
//  Weather
//
//  Created by Mindfire on 26/08/18.
//

#import "CityCell.h"

@interface CityCell()
@property (weak, nonatomic) IBOutlet UILabel *cityLbl;

@end
@implementation CityCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)bindViewModel:(id)viewModel {
    [self.cityLbl setText:[NSString stringWithFormat:@"%@, %@",[viewModel objectForKey:@"name"],[viewModel objectForKey:@"country"]]];
}

@end
