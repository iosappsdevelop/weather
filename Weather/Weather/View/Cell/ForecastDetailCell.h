//
//  ForecastDetailCell.h
//  Weather
//
//  Created by Mindfire on 23/08/18.
//

#import <UIKit/UIKit.h>
#import "WeatherViewDelegate.h"

@interface ForecastDetailCell : UITableViewCell <UICollectionViewDelegate ,UICollectionViewDataSource, WeatherViewDelegate>

@end
