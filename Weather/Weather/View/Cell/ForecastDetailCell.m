//
//  ForecastDetailCell.m
//  Weather
//
//  Created by Mindfire on 23/08/18.
//

#import "ForecastDetailCell.h"
#import "ForecastDetailsViewModel.h"
#import "ForecastCell.h"

@interface ForecastDetailCell()

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UILabel *cityNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *temeratureLbl;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLbl;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityView;

@property (strong, nonatomic) ForecastDetailsViewModel *viewModel;

@end

@implementation ForecastDetailCell 
static NSString * const kForecastCell = @"ForecastCell";

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.collectionView registerNib:[UINib nibWithNibName:kForecastCell bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:kForecastCell];
    self.view.layer.cornerRadius = 8;
    self.view.layer.masksToBounds = true;
    self.view.layer.borderWidth = 1.0f;
    self.view.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.viewModel.forecastViewModelArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    id<WeatherViewDelegate> cell = [collectionView dequeueReusableCellWithReuseIdentifier:kForecastCell forIndexPath:indexPath];
    
    [cell bindViewModel:[self.viewModel.forecastViewModelArray objectAtIndex:indexPath.row]];
    return (UICollectionViewCell *)cell;
}

- (void)bindViewModel:(id)viewModel {
    self.activityView.hidden = NO;
    [self.activityView startAnimating];
    if ([viewModel isKindOfClass:[City class]])
       [self.cityNameLbl setTextColor:[UIColor blueColor]];
    else
        [self.cityNameLbl setTextColor:[UIColor blackColor]];
    self.viewModel = [[ForecastDetailsViewModel alloc] initWithCityData:viewModel];
    self.viewModel.delegate = self;
}

- (void) updateWeatherResult {
    [self.cityNameLbl setText:self.viewModel.cityNameStr];
    [self.temeratureLbl setText:self.viewModel.temperatureStr];
    [self.descriptionLbl setText:self.viewModel.descriptionStr];
    [self.collectionView reloadData];
    if([self.activityView isAnimating]) {
        self.activityView.hidden = YES;
        [self.activityView stopAnimating];
    }
}

@end
