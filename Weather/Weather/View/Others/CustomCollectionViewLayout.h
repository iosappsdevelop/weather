//
//  CustomCollectionViewLayout.h
//  Weather
//
//  Created by Mindfire on 23/08/18.
//

#import <UIKit/UIKit.h>

@interface CustomCollectionViewLayout : UICollectionViewFlowLayout

-(instancetype)initWithItemNum:(NSUInteger)itemNum;

@end
