//
//  WebRequestManager.m
//  Weather
//
//  Created by Mindfire on 24/08/18.
//

#import "WebRequestManager.h"
#import <AFNetworking/AFHTTPSessionManager.h>
#import "NSDictionary+UrlEncoding.h"

@implementation WebRequestManager

+(void) getWebserviceRequestWithUrlStr:(NSString *)urlstr
                           WithParams:(NSDictionary *)params
                           WithResult:(void (^)(id result, NSError *error))completion
{
    
    NSString *requestURI = [NSString stringWithFormat:@"%@?%@",urlstr,[params urlEncodedString]];
    
    NSURL *requestUrl = [NSURL URLWithString:requestURI];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestUrl];
    request.HTTPMethod = @"GET";
    NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError * error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(!error) {
                NSError *error;
                NSDictionary* responseDictonary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                if(!error)
                    completion(responseDictonary, nil);

            }
            else
                completion(nil, error);
        });
    }];
    [dataTask resume];

}

@end
