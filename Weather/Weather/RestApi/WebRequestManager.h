//
//  WebRequestManager.h
//  Weather
//
//  Created by Mindfire on 24/08/18.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFHTTPSessionManager.h>

@interface WebRequestManager : NSObject
+(void) getWebserviceRequestWithUrlStr:(NSString *)urlstr
                            WithParams:(NSDictionary *)params
                            WithResult:(void (^)(id result, NSError *error))completion;
@end
