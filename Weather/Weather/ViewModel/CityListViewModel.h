//
//  CityListViewModel.h
//  Weather
//
//  Created by Mindfire on 26/08/18.
//

#import <Foundation/Foundation.h>
#import "WeatherViewDelegate.h"
#import "WeatherListViewModel.h"
#import "City.h"

@interface CityListViewModel : WeatherListViewModel

@property (nonatomic, strong) NSString *searchString;
@property BOOL isTableViewHidden;
@property BOOL isAddButtonEnabled;

- (void)addClicked;
- (void) citySelected: (NSInteger) index;
- (void) setSearchText:(NSString *)searchString;
@end
