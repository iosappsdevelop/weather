//
//  ForecastCellViewModel.m
//  Weather
//
//  Created by Mindfire on 25/08/18.
//

#import "ForecastCellViewModel.h"

@implementation ForecastCellViewModel

- (id)initWithForecastData:(ForecastData *) data {
    if (self = [super init]) {
        self.minTempString = [UtilityManager converKelvinToCelsius:data.minTemp];
        self.maxTempString = [UtilityManager converKelvinToCelsius:data.maxTemp];
        
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[data.date doubleValue]];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd"];
        self.dateString = [dateFormat stringFromDate:date];
        self.dayString = [UtilityManager convertDateToDay:date];
        
    }
    return self;
}

@end
