//
//  ForecastDetailsViewModel.m
//  Weather
//
//  Created by Mindfire on 24/08/18.
//

#import "ForecastDetailsViewModel.h"
#import "WeatherData.h"
#import "WeatherRequest.h"

@implementation ForecastDetailsViewModel {
    City *_city;
}

- (id)initWithCityData:(NSDictionary *) city {
    if (self = [super init]) {
        _city = [City intiWithDictioinary:city];
        [self initialize];
    }
    return self;
}

- (void) initialize {
    [self callToGetWeatherForecast];
}

- (void) callToGetWeatherForecast {
    WeatherRequest *requestData = [WeatherRequest new];
    requestData.cityId = _city.cityId;
    [[WeatherForecastService sharedInstance] getWeatherForecastBy:requestData withCompletionHandler:^(WeatherData *result, NSError *error) {
        self.cityNameStr = result.cityName;
       
        self.temperatureStr =  [UtilityManager converKelvinToCelsius:result.temp];
        self.descriptionStr = result.weatherDescription;
        self.forecastViewModelArray = [NSMutableArray new];
        for(ForecastData *data in result.forecastData) {
            [self.forecastViewModelArray addObject:[[ForecastCellViewModel alloc] initWithForecastData:data]];
        }
        [self.delegate updateWeatherResult];
    }];
}
@end
