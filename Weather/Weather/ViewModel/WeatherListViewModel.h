//
//  WeatherListViewModel.h
//  Weather
//
//  Created by Mindfire on 24/08/18.
//

#import <Foundation/Foundation.h>
#import "WeatherViewDelegate.h"
#import "LocationUtil.h"

@interface WeatherListViewModel : NSObject <MapDelegate>

@property (nonatomic, strong) NSMutableArray *results;
@property (nonatomic, weak) id <WeatherViewDelegate> delegate;
- (void)currentCity:(NSString *) city;
- (void) getCurrentLocation;
- (void)reloadData;
@end
