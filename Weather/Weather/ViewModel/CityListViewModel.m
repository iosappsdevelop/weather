//
//  CityListViewModel.m
//  Weather
//
//  Created by Mindfire on 26/08/18.
//

#import "CityListViewModel.h"
#import "CoreDataUtil.h"
#import "UtilityManager.h"

@implementation CityListViewModel {
    NSArray *_cityArray;
    City *_selectedCity;

}

- (id)init {
    if (self = [super init]) {
        [self initialize];
    }
    return self;
}

- (void) initialize {
    _cityArray = [[NSMutableArray alloc] init];

    _cityArray = [UtilityManager getCityArray];
    self.isTableViewHidden = YES;
    self.isAddButtonEnabled = NO;
    [self.delegate updateWeatherResult];
}

- (void) citySelected: (NSInteger) index {
    _selectedCity = [City taskWithManagedObject:[self.results objectAtIndex:index]];
    
    self.searchString = [NSString stringWithFormat:@"%@", _selectedCity.cityName];
    self.isTableViewHidden = YES;
    self.isAddButtonEnabled = YES;
    [self.delegate updateWeatherResult];
}

- (void) setSearchText:(NSString *)searchString {
    self.isTableViewHidden = [searchString isEqualToString:@""];
    self.isAddButtonEnabled = NO;
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF.name contains[c] %@", searchString];
    self.searchString = searchString;
    self.results = [[_cityArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
    [self.delegate updateWeatherResult];
}

- (void) addClicked {
    CoreDataUtil *data = [CoreDataUtil getInstance];
    NSArray *result = [[CoreDataUtil getInstance] fetchEntityWithName:@"CityEntity" withCondition:nil withSortCondition:nil withLimit:-1];
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF.id ==[c] %@", _selectedCity.cityId];
    result = [[result filteredArrayUsingPredicate:resultPredicate] mutableCopy];

    if(result.count == 0) {
        if (![data updateEntity:@"CityEntity" withCondition:nil withValue:[_selectedCity dictionary]]) {
            return;
        }
    }
    
    self.searchString = @"";
    self.isTableViewHidden = YES;
    self.isAddButtonEnabled = NO;
    [self.delegate updateWeatherResult];
}

@end
