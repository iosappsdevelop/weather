//
//  ForecastCellViewModel.h
//  Weather
//
//  Created by Mindfire on 25/08/18.
//

#import <Foundation/Foundation.h>
#import "WeatherViewDelegate.h"
#import "ForecastData.h"
#import "UtilityManager.h"

@interface ForecastCellViewModel : NSObject

@property (nonatomic, weak) id <WeatherViewDelegate> delegate;
@property (nonatomic, strong) NSString *dateString;
@property (nonatomic, strong) NSString *minTempString;
@property (nonatomic, strong) NSString *maxTempString;
@property (nonatomic, strong) NSString *dayString;
- (id)initWithForecastData:(ForecastData *) data;
@end
