//
//  WeatherListViewModel.m
//  Weather
//
//  Created by Mindfire on 24/08/18.
//

#import "WeatherListViewModel.h"
#import "CoreDataUtil.h"
#import "UtilityManager.h"
#import "City.h"

@implementation WeatherListViewModel {
    LocationUtil *_location;
}

- (id)init {
    if (self = [super init]) {
        [self initialize];
    }
    return self;
}

- (void) initialize {
    [self reloadData];
}

- (void)reloadData {
    self.results = [[[CoreDataUtil getInstance] fetchEntityWithName:@"CityEntity" withCondition:nil withSortCondition:nil withLimit:-1] mutableCopy];
    self.results = [[[self.results reverseObjectEnumerator] allObjects] mutableCopy];
    [self.delegate updateWeatherResult];
}

- (void) getCurrentLocation {
    _location = [[LocationUtil alloc] init];
    _location.delegate = self;
}

#pragma mark LocationUtil
- (void)currentCity:(NSString *) city {
    NSArray *cityArray = [UtilityManager getCityArray];
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF.name ==[c] %@", city];
    NSArray *resultArray = [[cityArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
    if(resultArray.count > 0) {
        if(!self.results)
            self.results = [[NSMutableArray alloc] init];
        [self.results insertObject:[City taskWithManagedObject:[resultArray firstObject]] atIndex:0];
        [self.delegate updateWeatherResult];
    }
}

@end
