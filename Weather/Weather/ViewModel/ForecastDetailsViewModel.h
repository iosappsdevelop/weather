//
//  ForecastDetailsViewModel.h
//  Weather
//
//  Created by Mindfire on 24/08/18.
//

#import <Foundation/Foundation.h>
#import "WeatherForecastService.h"
#import "WeatherViewDelegate.h"
#import "ForecastCellViewModel.h"
#import "City.h"

@interface ForecastDetailsViewModel : NSObject

@property (nonatomic, weak) id <WeatherViewDelegate> delegate;
@property (nonatomic, strong) NSString *temperatureStr;
@property (nonatomic, strong) NSString *cityNameStr;
@property (nonatomic, strong) NSString *descriptionStr;
@property (nonatomic, strong) NSMutableArray<ForecastCellViewModel *> *forecastViewModelArray;

- (id)initWithCityData:(City *) city;
@end
